import React from "react";
import { Text, View } from "react-native";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";

interface ButtonProps {
  text: string;
  onPress: () => void;
}

const Button: React.FC<ButtonProps> = (props) => {
  return (
    <TouchableWithoutFeedback onPress={props.onPress}>
      <View
        style={{
          borderRadius: 10,
          backgroundColor: "orange",
          minHeight: 40,
          margin: 5,
          padding: 5,
          alignItems: "center",
        }}
      >
        <View style={{ flex: 1 }} />
        <Text
          style={{
            color: "black",
            fontWeight: "bold",
          }}
        >
          {props.text}
        </Text>
        <View style={{ flex: 1 }} />
      </View>
    </TouchableWithoutFeedback>
  );
};

export default Button;
