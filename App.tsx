import React, { useState } from "react";
import { View } from "react-native";
import Button from "./Button";
import Follow from "./Follow";
import Gradient from "./Gradient";
import Starfield from "./Starfield";
import WavePath from "./WavePath";

const App: React.FC<{}> = () => {
  const [mode, setMode] = useState(0);

  return (
    <View style={{ flex: 1 }}>
      {mode === 0 && <Starfield />}
      {mode === 1 && <Gradient />}
      {mode === 2 && <WavePath />}
      {mode === 3 && <Follow />}
      <View
        pointerEvents="box-none"
        style={{ position: "absolute", width: "100%", height: "100%" }}
      >
        <View
          pointerEvents="box-none"
          style={{
            flex: 1,
            flexDirection: "row",
            alignItems: "flex-end",
            justifyContent: "space-around",
          }}
        >
          <Button text="Starfield" onPress={() => setMode(0)} />
          <Button text="Gradient" onPress={() => setMode(1)} />
          <Button text="Path" onPress={() => setMode(2)} />
          <Button text="Follow" onPress={() => setMode(3)} />
        </View>
        <View style={{ height: 50 }} />
      </View>
    </View>
  );
};

export default App;
