import React from "react";
import { View } from "react-native";
import Animated, { useAnimatedStyle } from "react-native-reanimated";

interface ParticleProps {
  x: Animated.SharedValue<number>;
  y: Animated.SharedValue<number>;
  size: number;
}

const Particle: React.FC<ParticleProps> = (props) => {
  const animStyle = useAnimatedStyle(() => {
    return {
      left: props.x.value - props.size / 2,
      top: props.y.value - props.size / 2,
    };
  });

  return (
    <Animated.View
      style={[
        {
          position: "absolute",
          width: props.size,
          height: props.size,
          backgroundColor: "black",
        },
        animStyle,
      ]}
    />
  );
};

export default Particle;
