import React, { useEffect } from "react";
import { Dimensions, View } from "react-native";
import {
  PanGestureHandler,
  PanGestureHandlerGestureEvent,
} from "react-native-gesture-handler";
import Animated, {
  useSharedValue,
  useAnimatedProps,
  useAnimatedGestureHandler,
  withSpring,
} from "react-native-reanimated";
import Svg, {
  Rect,
  Path,
  PathProps,
  Defs,
  Stop,
  LinearGradient,
} from "react-native-svg";

const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;
const speed = 0.02;
const startRotA = 0;
const startRotB = 90;

const AnimatedPath = Animated.createAnimatedComponent(Path);

const WavePath: React.FC<{}> = () => {
  const animRotA = useSharedValue(startRotA);
  const animRotB = useSharedValue(startRotB);

  const panEventHandler = useAnimatedGestureHandler<
    PanGestureHandlerGestureEvent,
    { startA: number; startB: number }
  >({
    onStart: (event, ctx) => {
      // Save current rotation values to gesture handler context
      ctx.startA = animRotA.value;
      ctx.startB = animRotB.value;
    },
    onActive: (event, ctx) => {
      // Adjust left and right rotation values from x and y translation
      const { translationX: x, translationY: y } = event;
      animRotA.value = ctx.startA + x * speed;
      animRotB.value = ctx.startB + y * speed;
    },
    onEnd: (event, ctx) => {
      // Snap back!
      animRotA.value = withSpring(startRotA);
      animRotB.value = withSpring(startRotB);
    },
  });

  const animatedPathProps = useAnimatedProps(() => {
    const x1 = windowWidth / 5;
    const y1 = windowHeight / 2;

    const x4 = (4 * windowWidth) / 5;
    const y4 = windowHeight / 2;

    const dist = windowWidth / 3;

    const x2 = x1 + Math.sin(animRotA.value) * dist;
    const y2 = y1 + Math.cos(animRotA.value) * dist;

    const x3 = x4 + Math.cos(animRotB.value) * dist;
    const y3 = y4 + Math.sin(animRotB.value) * dist;

    const path = `M ${x1},${y1} C ${x2},${y2} ${x3},${y3} ${x4},${y4} L ${x1},${y1} z`;
    const pathProps: PathProps = {
      stroke: 0xffee5577,
      strokeWidth: 8,
      fill: "none",
      d: path,
    };
    return pathProps;
  });

  return (
    <View style={{ flex: 1 }}>
      <PanGestureHandler onGestureEvent={panEventHandler}>
        <Animated.View style={{ flex: 1 }}>
          <Svg width="100%" height="100%">
            <Defs>
              <LinearGradient id="grad" x1="0" y1="0" x2="0" y2="1">
                <Stop stopColor="#887799" offset="0" stopOpacity="1" />
                <Stop stopColor="#EEAA99" offset="1" stopOpacity="1" />
              </LinearGradient>
            </Defs>
            <Rect width="100%" height="100%" fill="url(#grad)" />
            <AnimatedPath animatedProps={animatedPathProps} fill="black" />
          </Svg>
        </Animated.View>
      </PanGestureHandler>
      <Animated.View
        style={{
          position: "absolute",
          left: windowWidth / 4,
          top: windowHeight / 3,
          width: 20,
          height: 10,
          backgroundColor: "black",
        }}
      />
      <Animated.View
        style={{
          position: "absolute",
          left: (3 * windowWidth) / 4 - 20,
          top: windowHeight / 3,
          width: 20,
          height: 10,
          backgroundColor: "black",
        }}
      />
    </View>
  );
};

export default WavePath;
