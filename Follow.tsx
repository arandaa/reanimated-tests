import React from "react";
import { Dimensions, View } from "react-native";
import {
  PanGestureHandler,
  PanGestureHandlerGestureEvent,
  TapGestureHandler,
  TapGestureHandlerGestureEvent,
} from "react-native-gesture-handler";
import Animated, {
  useSharedValue,
  useAnimatedGestureHandler,
  withSpring,
} from "react-native-reanimated";
import Svg, { Rect, Defs, Stop, LinearGradient } from "react-native-svg";
import Particle from "./Particle";

const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;
const particleSize = 15;
const boxCount = 20;

const Follow: React.FC<{}> = () => {
  // Random value with 10% inset to avoid screen edges
  const random = (size: number) => (Math.random() * 0.8 + 0.1) * size;
  // Same but as a worklet so we can call it from animated hooks
  const randomWorklet = (size: number) => {
    "worklet";
    return (Math.random() * 0.8 + 0.1) * size;
  } 

  const boxX: Animated.SharedValue<number>[] = [];
  const boxY: Animated.SharedValue<number>[] = [];
  for (let i = 0; i < boxCount; ++i) {
    boxX.push(useSharedValue(random(windowWidth)))
    boxY.push(useSharedValue(random(windowHeight)))
  }

  const setTargetPos = (x: number, y: number) => {
    "worklet";
    for (let i = 0; i < boxCount; ++i) {
      boxX[i].value = withSpring(x);
      boxY[i].value = withSpring(y);
    }
  };

  const setRandomPos = () => {
    "worklet";
    for (let i = 0; i < boxCount; ++i) {
      boxX[i].value = withSpring(randomWorklet(windowWidth));
      boxY[i].value = withSpring(randomWorklet(windowHeight));
    }
  };

  const tapEventHandler = useAnimatedGestureHandler<TapGestureHandlerGestureEvent>(
    {
      onStart: (event, ctx) => {
        setTargetPos(event.x, event.y);
      },
      onEnd: (event, ctx) => {
        setRandomPos();
      },
    }
  );

  const panEventHandler = useAnimatedGestureHandler<
    PanGestureHandlerGestureEvent,
    { startX: number; startY: number }
  >({
    onStart: (event, ctx) => {},
    onActive: (event, ctx) => {
      setTargetPos(event.x, event.y);
    },
    onEnd: (event, ctx) => {
      setRandomPos();
    },
  });

  // attach animated props to an SVG path using animatedProps
  return (
    <View style={{ flex: 1 }}>
      <PanGestureHandler onGestureEvent={panEventHandler}>
        <Animated.View style={{ flex: 1 }}>
          <TapGestureHandler onGestureEvent={tapEventHandler}>
            <Animated.View style={{ flex: 1 }}>
              <Svg width="100%" height="100%">
                <Defs>
                  <LinearGradient id="grad" x1="0" y1="0" x2="0" y2="1">
                    <Stop stopColor="#77AA66" offset="0" stopOpacity="1" />
                    <Stop stopColor="#BB6688" offset="1" stopOpacity="1" />
                  </LinearGradient>
                </Defs>
                <Rect width="100%" height="100%" fill="url(#grad)" />
              </Svg>
              {boxX.map((_, i) => (
                <Particle key={i} x={boxX[i]} y={boxY[i]} size={particleSize} />
              ))}
            </Animated.View>
          </TapGestureHandler>
        </Animated.View>
      </PanGestureHandler>
    </View>
  );
};

export default Follow;
