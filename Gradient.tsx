import React from "react";
import { Dimensions, View } from "react-native";
import {
  PanGestureHandler,
  PanGestureHandlerGestureEvent,
} from "react-native-gesture-handler";
import Animated, {
  useSharedValue,
  useAnimatedProps,
  useAnimatedGestureHandler,
  withSpring,
} from "react-native-reanimated";
import Svg, {
  Rect,
  Defs,
  Stop,
  LinearGradient,
  LinearGradientProps,
} from "react-native-svg";


const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;

const AnimatedLinearGradient = Animated.createAnimatedComponent(LinearGradient);

const Gradient: React.FC<{}> = () => {
  const x1 = useSharedValue(200);
  const y1 = useSharedValue(200);
  const x2 = useSharedValue(200);
  const y2 = useSharedValue(600);
  const springConfig = { overshootClamping: false };

  const panEventHandler = useAnimatedGestureHandler<
    PanGestureHandlerGestureEvent
  >({
    onStart: (event, ctx) => {
      x1.value = withSpring(event.x, springConfig);
      y1.value = withSpring(event.y, springConfig);
    },
    onActive: (event, ctx) => {
      x2.value = withSpring(event.x, springConfig);
      y2.value = withSpring(event.y, springConfig);
    },
    onEnd: (event, ctx) => {},
  });

  const animatedGradientProps = useAnimatedProps(() => {
    const props: LinearGradientProps = {
      x1: x1.value / windowWidth,
      y1: y1.value / windowHeight,
      x2: x2.value / windowWidth,
      y2: y2.value / windowHeight,
    };
    return props;
  });

  return (
    <View style={{ flex: 1 }}>
      <PanGestureHandler onGestureEvent={panEventHandler}>
        <Animated.View style={{ flex: 1 }}>
          <Svg width="100%" height="100%">
            <Defs>
              <AnimatedLinearGradient
                animatedProps={animatedGradientProps}
                id="grad"               
              >
                <Stop stopColor="#55AABB" offset="0" stopOpacity="1" />
                <Stop stopColor="#FFD080" offset="1" stopOpacity="1" />
              </AnimatedLinearGradient>
            </Defs>
            <Rect width="100%" height="100%" fill="url(#grad)" />
          </Svg>
        </Animated.View>
      </PanGestureHandler>
    </View>
  );
};

export default Gradient;
